#include <ArduinoJson.h>
#include <Http.h>

#define RX_PIN 7
#define TX_PIN 8
#define RST_PIN 12
#define BATTERY_VOLTAGE_PIN A0
#define TEMP_PIN A1

HTTP http(9600, RX_PIN, TX_PIN, RST_PIN);
String UUID = "c1bca200-be5a-4725-b430-c5b2d72a1d60";
String BASE_URL = "https://my-wei-api.herokuapp.com/api/readings/";
String URL = BASE_URL + UUID;

void setup()
{
  // Serial
  Serial.begin(9600);
  Serial.println("Serial begin");

  // HTTP
  Result result = http.configureBearer("web.be");
  Serial.println(result);
  http.connect();  
}

void loop()
{
  delay(5000);

  // Readings
  double waterTemperature = (getVoltage(TEMP_PIN) * 100) - 50;
  Serial.println(waterTemperature);
  double batteryVoltage = getVoltage(BATTERY_VOLTAGE_PIN) * 15 / 5;
  Serial.println(batteryVoltage);

  // POST
  String jsonReading = 
    "{\"solarPanelVoltage\":" + String(batteryVoltage, 2) + "," + 
    "\"batteryVoltage\":" + String(batteryVoltage, 2) + "," + 
    "\"waterTemperature\":" + String(waterTemperature, 2) + "}";
  char url[URL.length()+1];
  URL.toCharArray(url, URL.length()+1);
  char json[jsonReading.length()+1]; // Convert to char for post function
  jsonReading.toCharArray(json, jsonReading.length()+1);
  char response[256];
  Result result = http.post(url, json, response);
  Serial.println(response);
}

double getVoltage(int ANALOG_PIN) {
  int reading = analogRead(ANALOG_PIN);
  return (reading * 5.0) / 1024;
}
