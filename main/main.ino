#include <SIM800L.h>
 
#define SIM800_TX_PIN 8
#define SIM800_RX_PIN 7
#define SIM800_RST_PIN 6
#define BATTERY_VOLTAGE_PIN A0
#define TEMP_PIN A1

String UUID = "c1bca200-be5a-4725-b430-c5b2d72a1d60";
String BASE_URL = "https://my-wei-api.herokuapp.com/api/readings/";
String URL = BASE_URL + UUID;
unsigned long interval = 1000L * 60L * 25L; // 30mins
unsigned long elapsedTime = 0;
bool postSuccessful = false;
char jsonReading[90];

SIM800L* sim800l;

void setup() {
  // Initialize Serial Monitor for debugging
  Serial.begin(115200);
  while(!Serial);
   
  // Initialize SIM800L driver with an internal buffer of 200 bytes and a reception buffer of 512 bytes, debug disabled
  sim800l = new SIM800L(SIM800_TX_PIN,SIM800_RX_PIN, SIM800_RST_PIN, 200, 200, false);

  // Setup module for GPRS communication
  setupModule();
}

void loop() {
  // Execute once and then every 30mins
  if (elapsedTime + interval > millis() && postSuccessful) {
    if (elapsedTime > millis()) elapsedTime = 0; // millis overflow after 50 days
    delay(20);
    return;
  }
  elapsedTime = millis();
  
  // Go into normal power mode
  bool normalPowerMode = sim800l->setPowerMode(NORMAL);
  if(normalPowerMode) {
    Serial.println(F("Module in normal power mode"));
  } else {
    Serial.println(F("Failed to switch module to normal power mode"));
  }
  
  // Establish GPRS connectivity (5 trials)
  bool connected = false;
  for(int i = 0; i < 5 && !connected; i++) {
    delay(1000);
    sim800l->disconnectGPRS();
    delay(1000);
    connected = sim800l->connectGPRS();
  }

  // Check if connected, if not reset the module and setup the config again
  if(connected) {
    Serial.println(F("GPRS connected !"));
  } else {
    Serial.println(F("GPRS not connected !"));
    Serial.println(F("Reset the module."));
    sim800l->reset();
    setupModule();
    return;
  }

  // Measures
  float batteryVoltage = getVoltage(BATTERY_VOLTAGE_PIN) * 15 / 5;
  float waterTemperature = (getVoltage(TEMP_PIN) * 100) - 50;

  // Create payload
  String json = "{\"solarPanelVoltage\": ";
  json += String(batteryVoltage, 2);
  json += ", \"batteryVoltage\": ";
  json += String(batteryVoltage, 2);
  json += ", \"waterTemperature\": ";
  json += String(waterTemperature, 2);
  json += "}";
  Serial.println(json);
  json.toCharArray(jsonReading, json.length()+1);

  // Do HTTP POST communication with 10s for the timeout (read and write)
  int rc = sim800l->doPost("https://my-wei-api.herokuapp.com/api/readings/c1bca200-be5a-4725-b430-c5b2d72a1d60", "application/json", jsonReading, 10000, 20000);
   if(rc == 200 || rc == 201) {
    Serial.println(F("HTTP POST SUCCESS"));
    postSuccessful = true;
  } else {
    Serial.print(F("HTTP POST error "));
    Serial.println(rc);
    postSuccessful = false;
  }

  // Go into low power mode
  bool lowPowerMode = sim800l->setPowerMode(MINIMUM);
  if(lowPowerMode) {
    Serial.println(F("Module in low power mode"));
  } else {
    Serial.println(F("Failed to switch module to low power mode"));
  }
}

double getVoltage(int ANALOG_PIN) {
  int reading = analogRead(ANALOG_PIN);
  return (reading * 5.0) / 1024;
}

void setupModule() {
    // Wait until the module is ready to accept AT commands
  while(!sim800l->isReady()) {
    Serial.println(F("Problem to initialize AT command, retry in 1 sec"));
    delay(1000);
  }
  Serial.println(F("Setup Complete!"));

  // Go into normal power mode
  bool normalPowerMode = sim800l->setPowerMode(NORMAL);
  if(normalPowerMode) {
    Serial.println(F("Module in normal power mode"));
  } else {
    Serial.println(F("Failed to switch module to normal power mode"));
  }

  // Wait for the GSM signal
  int signal = sim800l->getSignal();
  while(signal <= 0) {
    delay(1000);
    signal = sim800l->getSignal();
  }
  Serial.print(F("Signal OK (strenght: "));
  Serial.print(signal);
  Serial.println(F(")"));
  delay(1000);

  // Wait for operator network registration (national or roaming network)
  NetworkRegistration network = sim800l->getRegistrationStatus();
  while(network != REGISTERED_HOME && network != REGISTERED_ROAMING) {
    delay(1000);
    network = sim800l->getRegistrationStatus();
  }
  Serial.println(F("Network registration OK"));
  delay(1000);

  // Setup APN for GPRS configuration
  bool success = sim800l->setupGPRS("web.be");
  while(!success) {
    success = sim800l->setupGPRS("web.be");
    delay(5000);
  }
  Serial.println(F("GPRS config OK"));
  
}
